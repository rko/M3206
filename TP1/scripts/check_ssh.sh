#!/bin/bash

dpkg -l ssh >> /dev/null 2>&1

if [ $? -eq 0 ]; then
	echo "[...] ssh est installé[...]"
else
	echo "[/!\] ssh : le service n'est pas installé [/!\]"
fi

ps aux | grep [s]shd >> /dev/null 2>&1 # [s] pour ne pas afficher le process en lui même

if [ $? -eq 0 ]; then
	echo "[...] ssh:le service est lancé [...]"
else
	echo "[/!\] ssh : le service n'est pas lancé [/!\]"
	echo "Lancement du service ..."
	/etc/init.d/ssh start
fi
