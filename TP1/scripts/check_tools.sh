#!/bin/bash

#hash met en cache le resultat de la commande (git, tmux, ...) en mémoire.
#Cela retourne 0 si le programme est présent, 1 si le programme ne l'est pas

if hash git 2>/dev/null; then
	echo "[...] git : installé [...]"
else
	echo "[/!\] git pas installé [/!\ Lancer la commande 'apt-get install git'"
fi

if hash tmux 2>/dev/null; then
	echo "[...] tmux : installé [...]"
else
	echo "[/!\] tmux pas installé [/!\] Lancer la commande 'apt-get install tmux'"
fi


if hash vim 2>/dev/null; then
	echo "[...] vim : installé [...]"
else
	echo "[/!\] vim pas installé [/!\ Lancer la commande 'apt-get install vim'"
fi


if hash htop 2>/dev/null; then
	echo "[...] htop : installé [...]"

else
	echo "[/!\] htop pas installé [/!\ Lancer la commande 'apt-get install htop'"
fi
