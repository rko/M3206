#!/bin/bash
if [ "$EUID" -ne 0 ] #verification de l'etat root
	then echo "[/!\] Vous devez être super-utilisateur [/!\]"
else 
	/usr/bin/apt-get -qy update > /dev/null
	/usr/bin/apt-get -qy dist-upgrade > /dev/null
	echo "[...] Upgrade database [...]"
	echo "[...] Upgrade system [...]"
	exit 0
fi
