#!/bin/bash
echo "[...] Checking internet connection [...]"
wget -q --tries=10 --timeout=20 --spider http://google.com
if [[ $? -eq 0 ]]; then
	echo "[...] Internet Access OK [...]"
else
	echo "[/!\] Not connected to Internet [/!\]"
	echo "[/!\] Please check configuration [/!\]"
fi


# Wget sans OUTPUT, en 10 essai,  se comporte comme un Web spider
#il ne telecharge pas la page, mais verifie qu'elle est joignable

#Si la commande retourne 0 tout va bien
#Sinon il ny a pas internet
