#!/bin/bash

var=$( ls /tmp/backup/ -t | grep \.gz$ | head -1 )
mkdir -p /tmp/dossiertmp
cd /tmp/dossiertmp
tar -zxvf /tmp/backup/$var
mv /tmp/dossiertmp/* ~/rko/M3206/TP2/
