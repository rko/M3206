#!/bin/bash
FICHIER=../my_history
cat $FICHIER | awk 'BEGIN {FS=";"}{print $2}' | awk '{print $1}' | sort | uniq -c | sort -nr | head -n $1

#Le premier hawk découpe chaque ligne en deux parties, séparé par le ;
#Le deuxieme hawk permet de récuperer le premier mot de la chaine précédente
