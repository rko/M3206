#!/bin/bash

#date au format AAAA_MM_DD_HHMM
DATE=`date +%Y_%m_%d_%H%M`

#repertoire a sauvegarder passer en parametre
Repertoire=$1

mkdir -p /tmp/backup/
Destination="/tmp/backup"

#name = dernier repertoire de l'arborescence
Name=`basename $Repertoire`

#creation du fichier texte contenant le chemin absolu
touch $Repertoire/chemin_abs.txt
echo "$Repertoire" >> $Repertoire/chemin_abs.txt

#compression du dossier
tar -czf $Destination/$DATE-$Name.tar.gz -C $Repertoire/.. $Name
echo "Création de l'archive : $Destination/$Date-$Name.tar.gz"

#suppresion du fichier texte précédement créé
rm $Repertoire/chemin_abs.txt
